angular.module("app", ["ngResource", "ngRoute", "xeditable", "angularFileUpload"]).run(function($rootScope) {
    // adds some basic utilities to the $rootScope for debugging purposes
    $rootScope.log = function(thing) {
        console.log(thing);
    };

    $rootScope.alert = function(thing) {
        alert(thing);
    };
}).config(function($sceProvider) {
    //     // Completely disable SCE.  For demonstration purposes only!
    //     // Do not use in new projects.
    $sceProvider.enabled(false);
});