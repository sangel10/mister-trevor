angular.module("app").controller("ImageController", function($scope, $upload, $http) {

    var api_root = 'http://127.0.0.1:5984/rbma_images/'
    var _rev
    var imageID
    $scope.imageID


    $scope.createDocument = function() {
        var data = {}
        console.log('create document')

        return $http({
            method: 'POST',
            data: data,
            url: api_root,
            headers: {
                "Content-Type": "application/json"
            }
        })

    }

    $scope.addTags = function() {

        var url = api_root + imageID
        console.log('')

        $http.get(url)
            .then(function(results) {
                console.log('addtags get call results: ', results)
                // _rev = results.data._rev
                // var data = {
                //     "tags": $scope.tags.split(","),
                //     "_rev": _rev
                // }
                var data = results.data
                data["tags"] = $scope.tags.split(",");
                $http.put(url, data)
            })
            .then(function(results) {
                console.log('tags added')
            })

    }


    $scope.allImages = []

    $scope.loadImages = function() {
        $http.get('http://127.0.0.1:5984/rbma_images/_design/get-all/_view/get-all')
            .then(function(results) {
                console.log("load images", results)
                $scope.allImages = results.data.rows
            })

    }




    $scope.onFileSelect = function($files) {
        // var documentData = $scope.createDocument();
        // console.log('files: ', $files)
        //$files: an array of files selected, each file has name, size, and type.
        for (var i = 0; i < $files.length; i++) {

            // var url = 'http://127.0.0.1:5984/rbma_images/e3f3a0e34ef96c3a056543d8f1000f58'

            var file = $files[i];

            // $http.get(url)
            $scope.createDocument()
                .then(function(response) {
                    console.log(response)
                    // console.log("get request: ", response, response.data.blocks)
                    // _rev = response.data._rev;
                    // $scope.blocks = response.data.blocks;
                    _rev = response.data.rev
                    imageID = response.data.id
                }).then(function() {
                    console.log('_rev', _rev)
                    var imageUrl = api_root + imageID + "/image?rev=" + _rev
                    console.log("imageurl: ", imageUrl)
                    var fileReader = new FileReader();
                    fileReader.readAsArrayBuffer(file);
                    fileReader.onload = function(e) {
                        console.log(e.target.result)
                        $upload.http({
                            method: 'PUT',
                            url: imageUrl,
                            headers: {
                                'Content-Type': file.type
                            },
                            data: e.target.result
                        }).success(function(data) {
                            //success
                            // console.log('success: ', data);
                            $scope.block.content = 'http://127.0.0.1:5984/rbma_images/' + imageID + '/image'
                            console.log('success')
                        }).error(function(data) {
                            //error
                        });
                    }

                })

        }

        /* alternative way of uploading, send the file binary with the file's content-type.
       Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed. 
       It could also be used to monitor the progress of a normal http post/put request with large data*/
        // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
    };
})