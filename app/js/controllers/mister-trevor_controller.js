angular.module("app").controller("MisterTrevorController", function($scope, $http) {
    // because the stubbed endpoint returns an array of results, .query() is used
    // if the endpoint returned an object, you would use .get()
    // $scope.books = BookResource.query();
    $scope.state = {}
    $scope.state.showChoices = -2;
    // $scope.setShowChoices = function(num) {
    //     $scope.state.showChoices = num;
    // }
    $scope.clearChoices = function() {
        console.log("clear choices")
        $scope.state.showChoices = 0;
    }

    $scope.state.setChoice = function(choice) {

        $scope.state.showChoices = choice
    }

    $scope.state.removeBlock = function(index) {
        console.log("removing block: ", index)
        $scope.blocks.splice(index, 1);

    }

    $scope.currentTemplate;
    $scope.selectTemplate = function(template) {
        console.log(template)
        $scope.blocks = template.template;
        $scope.currentTemplate = template;
    }



    var eventTemplate = [{
        'type': 'header',
        'content': ''
    }, {
        'type': 'image',
        'content': ''
    }, {
        'type': 'text',
        'content': ''
    }, {
        'type': 'quote',
        'content': ''
    }]

    var interviewTemplate = [{
        'type': 'header',
        'content': ''
    }, {
        'type': 'quote',
    }, {
        'type': 'text',
    }, {
        'type': 'quote',
    }, {
        'type': 'text',
    }, ]

    var storyTemplate = [{
        'type': 'header',
    }, {
        'type': 'quote',
    }, {
        'type': 'image',
    }, {
        'type': 'text',
    }, {
        'type': 'image',
    }, ]

    $scope.templates = [{
        name: 'event',
        template: eventTemplate,
        fixed: true
    }, {
        name: 'story',
        template: storyTemplate
    }, {
        name: 'interview',
        template: interviewTemplate
    }, {
        name: 'freestyle',
        template: []
    }, ]

    // $scope.blocks = ['text', 'header', 'quote', 'image', 'audio', 'video']
    // 
    $scope.blocks = [{
        'type': 'text',
        'content': 'blah blah'
    }, {
        'type': 'header',
        'content': 'blah blah'
    }, {
        'type': 'quote',
        'content': 'blah blah',
        'cite': 'cicero'
    }, {
        'type': 'image',
        'content': 'blah blah'
    }, {
        'type': 'audio',
        'content': 'blah blah'
    }, {
        'type': 'video',
        'content': 'blah blah'
    }]

    $scope.blocks = []

    $scope.blockTypes = ['text', 'header', 'quote', 'image', 'audio', 'video']

    $http.get('http://127.0.0.1:5984/rbma_stories/d20ca5e959793b150eb11c2c2f000746')
        .then(function(response) {
            console.log("get request: ", response, response.data.blocks)
            $scope.blocks = response.data.blocks;
        })


    var _rev;
    $scope.updateDB = function() {
        console.log("update db clicked")
        $http.get('http://127.0.0.1:5984/rbma_stories/d20ca5e959793b150eb11c2c2f000746')
            .then(function(response) {
                console.log("get request: ", response, response.data.blocks)
                _rev = response.data._rev;
                // $scope.blocks = response.data.blocks;
            }).then(function() {
                $http.put('http://127.0.0.1:5984/rbma_stories/d20ca5e959793b150eb11c2c2f000746', {
                    'blocks': $scope.blocks,
                    '_rev': _rev
                })
                    .then(function(response) {
                        console.log("put succeeded: ", response)
                    })
                    .catch(function(response) {
                        console.log("put failed: ", response)
                    })

            })



    }


    $scope.state.addBlock = function(blockType) {
        console.log(blockType, $scope.state.showChoices)
        var data = {
            'type': blockType,
            'content': ''
        }
        $scope.blocks.splice($scope.state.showChoices + 1, 0, data)
        // $scope.state.showChoices = -5;
        // $scope.clearChoices();
        $scope.state.setChoice(-5)
        console.log($scope)
        console.log($scope.state.showChoices)
        // $scope.$parent.state = {}

    }


    $scope.mockData = [{
        "type": "quote",
        "data": {
            "cite": "asdfasdf",
            "text": "> asfasdfsdf"
        }
    }, {
        "type": "heading",
        "data": {
            "text": "This is my big header"
        }
    }, {
        "type": "text",
        "data": {
            "text": "this is my nice text"
        }
    }, {
        "type": "list",
        "data": {
            "text": " - this \n - is\n - my\n - list\n - !!\n - !\n"
        }
    }]


});