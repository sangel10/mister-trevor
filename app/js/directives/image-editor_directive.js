angular.module("app").directive("imageEditor", function() {
    return {
        restrict: 'E',
        templateUrl: 'image-test.html',
        controller: 'ImageController',
        scope: {
            block: "="
        }
        // template: '<h1>TEST</h1>'
    };
});