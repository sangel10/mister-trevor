angular.module("app").directive("preview", function() {
    return {
        restrict: 'E',
        templateUrl: 'preview.html'
    };
});