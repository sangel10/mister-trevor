angular.module("app").directive("addBlock", function() {
    return {
        restrict: 'E',
        templateUrl: 'add-block.html',
        scope: true,
    };
});